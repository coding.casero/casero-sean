console.log('meow')

for(i = 0; i < 20; i++) {
let movieLine ="Like tears in rain"
console.log(movieLine)

}
//declaring the function
function printThis() {

	console.log("Like tears in rain")
}
//calling teh function x times
printThis()
printThis()
printThis()
printThis()
printThis()
printThis()


function sayMyName(){
	console.log("Sean Sean")
};

sayMyName()


//function expression cannot be hoisted
let variableFunky = function() {
	console.log("hi there :3")
}

variableFunky()

//lexical scoping variables cannot be accessed when inside function and blocks

function punctionNames() {
	var punctionVar =("hi") 
	const puncConst =("hello")
	let puncLet =("huh")

	console.log(punctionVar)
console.log(puncConst)
console.log(puncLet)

}
punctionNames()

//nested punctions, punctions within punctions

function myNewPunction() {
	let name = "Jane";

	function nestedPunction() {
		let nestName = "johns"
		console.log(name)
	}
	nestedPunction()

	myNewPunction()
}

//using alert

function redAlert() {
	alert("sample alert")
}

redAlert()

//using prompt

function redPrompt() {
	alert("sample alert")
}
